package DeretAngka;

public class Soal05 {
    public static void Resolved(int n) {
        int helper1 = 0;
        int helper2 = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            if (helper1 == 2) {
                hasil[i] = 0;
                helper1 -= 2;
            } else {
                hasil[i] = helper2;
                helper2 += 4;
                helper1++;
            }
        }

        Utility.BintangPrint(hasil);
    }
}
