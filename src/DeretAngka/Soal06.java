package DeretAngka;

public class Soal06 {
    public static void Resolved(int n) {
        int helper1 = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = helper1;
            helper1 += 4;
        }

        Utility.Bintang(hasil);
    }
}
