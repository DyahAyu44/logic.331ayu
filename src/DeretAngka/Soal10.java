package DeretAngka;

public class Soal10 {
    public static void Resolved(int n) {
        int helper1 = 3;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = helper1;
            helper1 *= 3;
        }

        Utility.PrintX(hasil);
    }
}
