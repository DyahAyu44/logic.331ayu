package DeretAngka;

public class Soal11 {
    public static void Resolved(int n) {
        int helper = 1;
        int before = 0;
        int after = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = helper;
            helper = before + after;
            before = after;
            after = helper;

        }


        Utility.PrintArray1D(hasil);
    }
}
