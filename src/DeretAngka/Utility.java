package DeretAngka;

public class Utility {
    public static void PrintArray1D(int[] hasil) {
        for (int i = 0; i < hasil.length; i++) {
            System.out.print(hasil[i] + " ");

        }
        System.out.println("");
    }

    public static void Bintang(int[] hasil) {
        for (int i = 0; i < hasil.length; i++) {
            if (hasil[i] % 3 == 0) {
                System.out.print("* ");
            } else {
                System.out.print(hasil[i] + " ");
            }
        }
        System.out.println("");
    }

    public static void BintangPrint(int[] hasil) {
        for (int i = 0; i < hasil.length; i++) {
            if (hasil[i] == 0) {
                System.out.print("* ");
            } else {
                System.out.print(hasil[i] + " ");
            }
        }
        System.out.println("");
    }

    public static void PrintX(int[] hasil) {
        int helper = 1;
        for (int i = 0; i < hasil.length; i++) {
            if (helper % 4 == 0) {
                System.out.print("XXX ");
                helper ++;
            } else {
                System.out.print(hasil[i] + " ");
                helper ++;
            }
        }
        System.out.println("");
    }
}
