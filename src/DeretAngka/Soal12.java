package DeretAngka;

public class Soal12 {
    public static void Resolved(int n) {
        int helper = 1;
        boolean flag = true;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            while (flag){
                if (helper == 1){
                    helper++;
                }
                else if (helper == 2) {
                    results[i] = helper;
                    helper++;
                    flag = false;
                }
                else if (helper == 3) {
                    results[i] = helper;
                    helper++;
                    flag = false;
                }
                else if (helper % 2 != 0 && helper % 3 != 0) {
                    results[i] = helper;
                    helper++;
                    flag = false;
                }
                else {
                    helper++;
                }
            }
            flag = true;
        }

        Utility.PrintArray1D(results);
    }
}
