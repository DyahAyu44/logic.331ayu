package DeretAngka;

public class Soal09 {
    public static void Resolved(int n) {
        int helper1 = 4;
        int helper2 = 4;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            if (helper1 % 3 == 0) {
                hasil[i] = 0;
                helper1++;
            } else {
                hasil[i] = helper2;
                helper2 *= 4;
                helper1++;
            }
        }

        Utility.Bintang(hasil);
    }
}
