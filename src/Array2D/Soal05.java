package Array2D;

import java.util.Scanner;

public class Soal05 {

    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        int[][] hasil = new int[3][n];
        int helper1 = 0;
        int helper2 = n;
        int helper3 = helper2 + n;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    hasil[i][j] = helper1;
                    helper1 ++;
                } else if (i == 1) {
                    hasil[i][j] = helper2;
                    helper2 ++;
                } else if (i == 2) {
                    hasil[i][j] = helper3;
                    helper3 ++;
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
