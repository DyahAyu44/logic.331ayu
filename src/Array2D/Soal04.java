package Array2D;

import java.util.Scanner;

public class Soal04 {

    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        int[][] hasil = new int[2][n];
        int helper1 = 0;
        int helper2 = 1;
        int helper3 = 5;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    hasil[i][j] = helper1;
                    helper1 ++;
                } else if (i == 1) {
                    if ((j+1) % 2 == 1){
                        hasil[i][j] = helper2;
                        helper2 ++;
                    } else {
                        hasil[i][j] = helper3;
                        helper3 += 5;
                    }
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
