package Array2D;

import java.util.Scanner;

public class Soal06 {

    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        int[][] hasil = new int[3][n];
        int helper1 = 0;
        int helper2 = 1;
        int helper3 = 0;
        int helper4 = 1;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    hasil[i][j] = helper1;
                    helper1 ++;
                } else if (i == 1) {
                    hasil[i][j] = helper2;
                    helper2 *= n;
                } else if (i == 2) {
                    hasil[i][j] = helper3 + helper4;
                    helper3 ++;
                    helper4 *= n;
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
