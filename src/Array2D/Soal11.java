package Array2D;

import java.util.Scanner;

public class Soal11 {

    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        int[][] hasil = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((j + i) >= n -1 )
                {
                    hasil[i][j] = 0;
                } else
                {
                    hasil[i][j] = 1;
                }
            }
        }
        Utility.BintangPrint(hasil);
    }
}
