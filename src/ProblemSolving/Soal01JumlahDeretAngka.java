package ProblemSolving;

import java.util.Scanner;

public class Soal01JumlahDeretAngka {

    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();
        int[] deret1 = new int[n];
        int[] deret2 = new int[n];

        for (int i = 0; i < deret1.length; i++) {
            deret1[i] = i * 3 - 1;
            System.out.print(deret1[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < deret2.length; i++) {
            deret2[i] = i * (-2 * 1);
            System.out.print(deret2[i] + " ");
        }
        System.out.println();

        int[] deret3 = new int[n];

        for (int i = 0; i < deret3.length; i++) {
            deret3[i] = deret1[i] + deret2[i];
            System.out.print(deret3[i] + " ");
        }
        System.out.println();
    }
}
