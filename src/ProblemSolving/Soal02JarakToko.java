package ProblemSolving;

import InputPlural.Utility;

import java.util.Scanner;

public class Soal02JarakToko {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Jarak Grosir X ke Toko : ");
        System.out.println("1. Toko 1 : 0.5 km");
        System.out.println("2. Toko 2 : 2 km");
        System.out.println("3. Toko 3 : 3.5 km");
        System.out.println("4. Toko 4 : 5 km");

        System.out.print("Masukkan Rute yang Diinginkan : ");
        String rute = input.nextLine();
        String jarak = ("0 0.5 2 3.5 5");
        double jarakTempuh = 0;
        double selisihJarak = 0;

        int[] ruteToko = Utility.ConvertStringToArrayInt(rute);
        double[] jarakToko = Utility.ConvertStringToArrayDouble(jarak);

        for (int i = 0; i < ruteToko.length; i++) {
            if (i == 0)
            {
                jarakTempuh = jarakToko[ruteToko[i]];
            }
            else if (i == ruteToko.length - 1)
            {
                selisihJarak = jarakToko[ruteToko[i]] - jarakToko[ruteToko[i] - 1];
                if (selisihJarak < 0)
                {
                    selisihJarak *= -1;
                }
                jarakTempuh += (selisihJarak + jarakToko[ruteToko[i]]);
            } else
            {
                selisihJarak = jarakToko[ruteToko[i]] - jarakToko[ruteToko[i] - 1];
                if (selisihJarak < 0)
                {
                    selisihJarak *= -1;
                }
                jarakTempuh += selisihJarak;
            }
        }

        System.out.println("Jarak yang ditempuh : " + jarakTempuh + " KM");
        int waktu = (int) ((jarakTempuh * 2) + (ruteToko.length * 10));
        System.out.println("Waktu yang diperlukan : " + waktu + " menit");
    }
}
