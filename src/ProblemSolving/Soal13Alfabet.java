package ProblemSolving;

import java.util.Scanner;

public class Soal13Alfabet {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        String string, array;

        System.out.println("Example : ");
        System.out.println("String : abcdzzz");
        System.out.println("Array : [1, 2, 2, 4, 4, 26, 26]");

        System.out.print("String : ");
        string = input.nextLine();
        System.out.print("Array : ");
        array = input.nextLine();

        char[] stringArray = string.toCharArray();
        String[] arraySplit = array.split(", ");
        int[] arrayAngka = new int[arraySplit.length];

        for (int i = 0; i < arrayAngka.length; i++) {
            arrayAngka[i] = Integer.parseInt(arraySplit[i]);
        }

        String[] result = new String[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            if (stringArray[i] - 96 == arrayAngka[i]) {
                result[i] = "true";
            } else {
                result[i] = "false";
            }
        }

        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }
        System.out.println();
    }
}
