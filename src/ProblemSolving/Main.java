package ProblemSolving;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        boolean flag = true;
        String answer = "y";

        while (flag) {
            System.out.println("Pilih Soal (1 - 14) : ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 14) {
                System.out.println("Angka tidak tersedia");
                pilihan = input.nextInt();
            }

            switch (pilihan) {
                case 1:
                    Soal01JumlahDeretAngka.Resolve();
                    break;
                case 2:
                    Soal02JarakToko.Resolve();
                    break;
                case 3:
                    Soal03JumlahBuah.Resolve();
                    break;
                case 4:
                    Soal04Andi.Resolve();
                    break;
                case 5:
                    Soal05ConvertJam.Resolve();
                    break;
                case 6:
                    Soal06PesanOnline.Resolve();
                    break;
                case 7:
                    Soal07Jim.Resolve();
                    break;
                case 8:
                    Soal08JumlahAndi.Resolve();
                    break;
                case 9:
                    Soal09Parkir.Resolve();
                    break;
                case 10:
                    Soal10KonversiBotol.Resolve();
                    break;
                case 11:
                    Soal11Pulsa.Resolve();
                    break;
                case 12:
                    Soal12JumlahBuah.Resolve();
                    break;
                case 13:
                    Soal13Alfabet.Resolve();
                    break;
                default:

            }
            System.out.println("Try again? y/n");
            input.nextLine(); //for skip
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")) {
                flag = false;
            }
        }
    }
}