package ProblemSolving;

import java.util.Scanner;

public class Soal11Pulsa {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Explain : ");
        System.out.println("0 - 10.000 - > 0 point");
        System.out.println("10.001 - 30.000 -> 1 point setiap kelipatan 1.000");
        System.out.println("> 30.000 -> 2 point setiap kelipatan 1.000");

        System.out.println();

        System.out.println("Example : ");
        System.out.println("Beli pulsa Rp. 20.000");
        System.out.println("0 - 10.00 -> 0 point");
        System.out.println("10.001 - 20.000 -> 10.000 / 1000 = 10 point");
        System.out.println("Output : 0 + 10 = 10 point");

        System.out.println();

        int pulsa, point = 0;

        System.out.print("Beli Pulsa Rp. ");
        pulsa = input.nextInt();

        if (pulsa < 10000) {
            point = 0;
        } else if (pulsa > 10000 && pulsa <= 30000) {
            point = (pulsa - 10000) / 1000;
        } else {
            point = (pulsa - 30000) / 1000 * 2;
            point += 20;
        }
        System.out.print("Total Point : " + point);
        System.out.println();
    }
}
