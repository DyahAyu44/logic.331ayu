package ProblemSolving;

import java.util.Scanner;

public class Soal05ConvertJam {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Example : ");
        System.out.println("Input : 12:35 AM");
        System.out.println("Output : 00:35");
        System.out.println("Input : 19:30");
        System.out.println("Output : 07:30 PM");

        String jam;

        System.out.print("Input Jam : ");
        jam = input.nextLine();

        String jamSubstring = jam.substring(0,2);
        int jamInt = Integer.parseInt(jamSubstring);
        String konversi = Integer.toString(jamInt);

        if (jam.contains("am")) {
            if (jamInt >= 12) {
                jamInt -= 12;
                jam = jam.replace(jamSubstring, konversi);
                jam = jam.replace("am", "");
                System.out.println(jamInt + ":" + jam.substring(3,5));
            } else if (jamInt < 12) {
                jam = jam.replace("am", "");
                System.out.println(jam);
            }
        } else if (jam.contains("pm")) {
            if (jamInt <= 12) {
                jamInt += 12;
                jam = jam.replace(jamSubstring, konversi);
                jam = jam.replace("pm", "");
                System.out.println(jamInt + ":" + jam.substring(3,5));
            } else if (jamInt > 12) {
                jam = jam.replace("pm", "");
                System.out.println(jam);
            }
        } else if (jamInt < 24 && jamInt > 12) {
            jamInt -= 12;
            jam = jam.replace(jamSubstring, konversi);
            System.out.println(jamInt + ":" + jam.substring(3,5) + " pm");
        } else if (jamInt < 12 && jamInt >= 0) {
            jam = jam.replace(jamSubstring, konversi);
            System.out.println(jam + " am");
        }
    }
}
