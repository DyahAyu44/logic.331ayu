package ProblemSolving;

import java.util.Scanner;

public class Soal04Andi {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int uang, jumlahBarang;

        System.out.println("Membeli Barang");
        System.out.println("Masukkan Uang : ");
        uang = input.nextInt();
        System.out.println("Jumlah Barang : ");
        jumlahBarang = input.nextInt();
        input.nextLine();

        String[] barang = new String[jumlahBarang];
        int[] hargaBarang = new int[jumlahBarang];

        for (int i = 0; i < jumlahBarang; i++) {
            System.out.println("Nama Barang");
            barang[i] = input.nextLine();
            System.out.println("Harga Barang");
            hargaBarang[i] = input.nextInt();
            input.nextLine();
        }

        for (int i = 0; i < jumlahBarang; i++) {
            if (uang >= hargaBarang[i]) {
                System.out.println(barang[i] + ", " + hargaBarang[i]);
                uang -= hargaBarang[i];
            }
        }
        System.out.println();
    }
}
