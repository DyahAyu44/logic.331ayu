package ProblemSolving;

import java.util.Scanner;

public class Soal07Jim {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        String pola, jalan;

        System.out.println("- Melambangkan Jalan");
        System.out.println("o Melambangkan Lubang");
        System.out.println("Ex Inputan : -----o---o--");
        System.out.print("Pola Lintasan : ");
        pola = input.nextLine();

        System.out.println("w Melambangkan Jalan");
        System.out.println("j Melambangkan Lompat");
        System.out.println("Ex Inputan : wwwwwjwwwjww");
        System.out.print("Pilihan Cara Berjalan : ");
        jalan = input.nextLine().toLowerCase();

        int energi = 0;
        boolean jatuh = false;
        int helper = 0;

        for (int i = 0; i < jalan.length(); i++) {
            if (jalan.charAt(i) == 'w' && pola.charAt(helper) == '-') {
                energi ++;
                helper ++;
            } else if (jalan.charAt(i) == 'w' && pola.charAt(helper) == 'o') {
                jatuh = true;
                break;
            } else if (jalan.charAt(i) == 'j' && energi >= 2) {
                helper += 2;
                if (pola.charAt(helper - 1) == 'o') {
                    jatuh = true;
                    break;
                } else {
                    energi -= 2;
                }
            } else if (jalan.charAt(i) == 'j' && jalan.charAt(helper) == 'o' && energi < 0) {
                jatuh = true;
                break;
            }
        }
        if (jatuh) {
            System.out.println("Jim Died");
        }
        if (!jatuh) {
            System.out.println(energi);
        }
    }
}
