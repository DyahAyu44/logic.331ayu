package ProblemSolving;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Soal09Parkir {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String masuk, keluar;

        long durasiParkir = 0, jamParkir, biayaParkir;
        Date dateMasuk = null , dateKeluar = null;
        System.out.print("Tanggal dan Jam Masuk : ");
        masuk = input.nextLine();
        System.out.print("Tanggal dan Jam Keluar : ");
        keluar = input.nextLine();

        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
        try {
            dateMasuk = dateFormat.parse(masuk);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        try {
            dateKeluar = dateFormat.parse(keluar);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        durasiParkir = dateKeluar.getTime() - dateMasuk.getTime();
        jamParkir = durasiParkir / (1000 * 60 * 60);

        if (jamParkir <= 8) {
            biayaParkir = jamParkir * 1000;
        } else if (jamParkir > 8 && jamParkir <= 24) {
            biayaParkir = 8000;
        } else {
            biayaParkir = (jamParkir / 24) * 15000;
        }

        System.out.print("Output : " + biayaParkir);
        System.out.println();
    }
}
