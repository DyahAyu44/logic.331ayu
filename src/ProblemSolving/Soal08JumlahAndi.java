package ProblemSolving;

import java.util.Scanner;
import java.util.Arrays;

public class Soal08JumlahAndi {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        int uangAndi;
        String hargaKacamata, hargaBaju;

        System.out.println("Contoh : ");
        System.out.println("Uang Andi : 70");
        System.out.println("Harga Kacamata : 43, 26, 44");
        System.out.println("Harga Baju : 21, 39, 33");

        System.out.print("Uang Andi : ");
        uangAndi = input.nextInt();
        input.nextLine();

        System.out.print("Harga Kacamata : ");
        hargaKacamata = input.nextLine();

        System.out.print("Harga Baju : ");
        hargaBaju = input.nextLine();


        String[] splitKacamata = hargaKacamata.split(", ");
        int[] itemKacamata = new int[splitKacamata.length];

        for (int i = 0; i < splitKacamata.length; i++) {
            itemKacamata[i] = Integer.parseInt(splitKacamata[i]);
        }

        String[] splitBaju = hargaBaju.split(", ");
        int[] itemBaju = new int[splitBaju.length];

        for (int i = 0; i < splitBaju.length; i++) {
            itemBaju[i] = Integer.parseInt(splitBaju[i]);
        }

//        System.out.println("check kacamata: ");
//        for (String k : splitKacamata) {
//            System.out.print(k + "|\n");
//        }
//
//        System.out.println("check baju: ");
//        for (String b : splitBaju) {
//            System.out.print(b + "|");

//        }

        int[] kombinasiHarga = new int[itemKacamata.length * itemBaju.length];
        int helper = 0;
        for (int i = 0; i < itemKacamata.length; i++) {
            for (int j = 0; j < itemBaju.length; j++) {
                kombinasiHarga[helper] = itemKacamata[i] + itemBaju[j];
                helper++;
            }
        }

//        for (int i = 0; i < kombinasiHarga.length; i++) {
//            System.out.print(kombinasiHarga + " ");
//        }

        Arrays.sort(kombinasiHarga);
        int helperUang = 0;
        for (int i = 0; i < kombinasiHarga.length; i++) {
            if (kombinasiHarga[i] <= uangAndi) {
                helperUang = kombinasiHarga[i];
            }
        }

        System.out.println("Gabungan Kacamata dan Baju Termahal adalah : " + helperUang);
    }
}
