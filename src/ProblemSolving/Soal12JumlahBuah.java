package ProblemSolving;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Soal12JumlahBuah {

    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        HashMap<String, Integer> fruit = new HashMap<>();

        System.out.println("Input Penjualan : ");
        System.out.println("Contoh Inputan : Apel:1, Pisang:3, Jeruk:1, Apel:3, Apel:5, Jeruk:8, Mangga:1");
        System.out.print("Input : ");
        String buah = input.nextLine();

        String[] dataSplit = buah.split(", ");
        String[] item = new String[2];

        for (int i = 0; i < dataSplit.length; i++) {
            item = dataSplit[i].split(":");
            if (fruit.get(item[0]) == null)
            {
                fruit.put(item[0], Integer.parseInt(item[1]));
            }
            else
            {
                int helper = fruit.get(item[0]);
                fruit.put(item[0], Integer.parseInt(item[1]) + helper);
            }
        }

        //System.out.println(fruit);

        String[] results = new String[fruit.size()];
        int index = 0;
        for (String key : fruit.keySet()) {
            results[index] = key;
            index++;
        }
        Arrays.sort(results);

        for (String key : results) {
            System.out.println(key + ": " + fruit.get(key));
        }
    }
}
