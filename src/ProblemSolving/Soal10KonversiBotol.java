package ProblemSolving;

import java.util.Scanner;
public class Soal10KonversiBotol {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        String wadah, wadahKonversi;
        int jumlah;
        double konversi;
        boolean flag = true;

        while (flag) {
        System.out.println("Sistem Konversi Volume");
        System.out.println("Jika 1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2.5 cangkir");

        System.out.println("Masukkan Sistem yang Akan Dikonversi : ");
        System.out.println("Contoh : botol");
        wadah = input.nextLine();

        System.out.println("Masukkan Jumlah yang Akan Dikonversi : ");
        System.out.println("Contoh : 2");
        jumlah = input.nextInt();

        System.out.println("Ingin Dikonversi ke Dalam Bentuk : ");
        System.out.println("Contoh : gelas");
        input.nextLine();
        wadahKonversi = input.nextLine();


            // botol
            if (wadah.equals("botol") && wadahKonversi.equals("gelas")) {
                konversi = jumlah * 2;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("botol") && wadahKonversi.equals("teko")) {
                konversi = jumlah / 5;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("botol") && wadahKonversi.equals("cangkir")) {
                konversi = jumlah * 5;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
                // gelas
            } else if (wadah.equals("gelas") && wadahKonversi.equals("botol")) {
                konversi = jumlah / 2;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("gelas") && wadahKonversi.equals("teko")) {
                konversi = jumlah / 10;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("gelas") && wadahKonversi.equals("cangkir")) {
                konversi = jumlah * 2.5;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
                // teko
            } else if (wadah.equals("teko") && wadahKonversi.equals("botol")) {
                konversi = jumlah * 5;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("teko") && wadahKonversi.equals("gelas")) {
                konversi = jumlah * 10;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("teko") && wadahKonversi.equals("cangkir")) {
                konversi = jumlah * 25;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
                //cangkir
            } else if (wadah.equals("cangkir") && wadahKonversi.equals("botol")) {
                konversi = jumlah / 5;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("cangkir") && wadahKonversi.equals("gelas")) {
                konversi = jumlah / 2.5;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else if (wadah.equals("cangkir") && wadahKonversi.equals("teko")) {
                konversi = jumlah / 25;
                System.out.println(jumlah + " " + wadah + " = " + konversi + " " + wadahKonversi);
                flag = false;
            } else {
                System.out.println("Konversi Tidak Tersedia");
                flag = true;
            }
        }
    }
}
