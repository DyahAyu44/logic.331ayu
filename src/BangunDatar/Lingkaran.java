package BangunDatar;
import java.util.Scanner;
public class Lingkaran
{
    private static Scanner input = new Scanner(System.in);

    private static double phi;
    private static double jarijari;

    public static void Luas()
    {
        // Luas Lingkaran
        System.out.println("Menghitung Luas Lingkaran");

        phi = 3.14;

        System.out.print("Input Jari Jari = ");
        jarijari = input.nextDouble();

        double luas_lingkaran = phi * jarijari * jarijari;

        System.out.println("Luas Lingkaran = " +luas_lingkaran);
        System.out.println();
    }

    public static void Keliling()
    {
        // Keliling Lingkaran
        System.out.println("Menghitung Keliling Lingkaran");

        phi = 3.14;

        System.out.print("Input Jari Jari = ");
        jarijari = input.nextDouble();

        double keliling_lingkaran = 2 * phi * jarijari;

        System.out.println("Keliling Lingkaran = " + keliling_lingkaran);
        System.out.println();
    }
}
