package BangunDatar;
import java.util.Scanner;
public class PersegiPanjang
{
    private static Scanner input = new Scanner(System.in);

    private static int panjang;
    private static int lebar;

    public static void Luas()
    {
        // Luas Persegi Panjang
        System.out.println("Menghitung Luas Persegi Panjang");

        System.out.print("Input Panjang = ");
        panjang = input.nextInt();

        System.out.print("Input Lebar = ");
        lebar = input.nextInt();

        int luas = panjang * lebar;

        System.out.println("Luas persegi panjang = " + luas);
        System.out.println();
    }

    public static void Keliling()
    {
        // Keliling Persegi Panjang
        System.out.println("Menghitung Keliling Persegi Panjang");

        System.out.print("Input Panjang = ");
        panjang = input.nextInt();

        System.out.print("Input Lebar = ");
        lebar = input.nextInt();

        int keliling = 2 * (panjang + lebar);

        System.out.println("Keliling persegi panjang = " + keliling);
        System.out.println();
    }
}
