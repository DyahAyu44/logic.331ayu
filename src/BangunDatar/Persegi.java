package BangunDatar;
import java.util.Scanner;
public class Persegi
{
    private static Scanner input = new Scanner(System.in);

    private static int sisi;

    public static void Luas()
    {
        // Luas Persegi
        System.out.println("Menghitung Luas Persegi");

        System.out.print("Input Sisi = ");
        sisi = input.nextInt();

        int luasPersegi = sisi * sisi;

        System.out.println("Luas Persegi = " + luasPersegi);
        System.out.println();
    }

    public static void Keliling()
    {
        // Keliling Persegi
        System.out.println("Menghitung Keliling Persegi");

        System.out.print("Input Sisi = ");
        sisi = input.nextInt();

        int kelilingPersegi = 4 * sisi;

        System.out.println("Keliling Persegi = " + kelilingPersegi);
        System.out.println();
    }
}
