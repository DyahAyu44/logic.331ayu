package BangunDatar;
import java.util.Scanner;
public class Trapesium
{
    private static Scanner input = new Scanner(System.in);

    private static int alas_a;
    private static int alas_b;
    private static int tinggi;
    private static int sisi_a;
    private static int sisi_b;

    public static void Luas()
    {
        // Luas Trapesium
        System.out.println("Menghitung Luas Trapesium");

        System.out.print("Input Alas A = ");
        alas_a = input.nextInt();

        System.out.print("Input Alas B = ");
        alas_b = input.nextInt();

        System.out.print("Input Tinggi = ");
        tinggi = input.nextInt();

        double luas_trapesium = 0.5 * (alas_a + alas_b) * tinggi;

        System.out.println("Luas Trapesium = " + luas_trapesium);
        System.out.println();
    }

    public static void Keliling()
    {
        // Keliling Trapesium
        System.out.println("Menghitung Keliling Trapesium");

        System.out.print("Input Alas A = ");
        alas_a = input.nextInt();

        System.out.print("Input Alas B = ");
        alas_b = input.nextInt();

        System.out.print("Input Sisi Miring A = ");
        sisi_a = input.nextInt();

        System.out.print("Input Sisi Miring B = ");
        sisi_b = input.nextInt();

        int keliling_trapesium = alas_a + alas_b + sisi_a + sisi_b;

        System.out.println("Keliling Trapesium = " + keliling_trapesium);
        System.out.println();
    }
}
