package BangunDatar;
import java.util.Scanner;
public class Segitiga
{
    private static Scanner input = new Scanner(System.in);

    private static int alas;
    private static int tinggi;
    private static int miring;

    public static void Luas()
    {
        // Luas Segitiga
        System.out.println("Menghitung Luas Segitiga");

        System.out.print("Input Alas = ");
        alas = input.nextInt();

        System.out.print("Input Tinggi = ");
        tinggi = input.nextInt();

        double luas_segitiga = 0.5 * alas * tinggi;

        System.out.println("Luas Segitiga = " + luas_segitiga);
        System.out.println();
    }

    public static void Keliling()
    {
        // Keliling Segitiga
        System.out.println("Menghitung Keliling Segitiga");

        System.out.print("Input Alas = ");
        alas = input.nextInt();

        System.out.print("Input Tinggi = ");
        tinggi = input.nextInt();

        System.out.print("Input Sisi Miring = ");
        miring = input.nextInt();

        int keliling_segitiga = alas + tinggi + miring;

        System.out.println("Keliling Segitiga = " + keliling_segitiga);
        System.out.println();
    }
}
