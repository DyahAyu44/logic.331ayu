package InputPlural;

import java.util.Arrays;
import java.util.Scanner;

public class Soal07JumlahTerbesar {

    public static void Resolved() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Deret Angka : ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int min = 0;
        int max = 0;

        Arrays.sort(intArray);
        for (int i = 0; i < length-1; i++) {
            min = min + intArray[i];
        }
        for (int i = 1; i < length; i++) {
            max = max + intArray[i];
        }
        System.out.print("Terbesar : " + max);
        System.out.println();
        System.out.println("Terkecil : " + min);
        System.out.println();

    }
}
