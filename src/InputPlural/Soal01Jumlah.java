package InputPlural;

import java.util.Scanner;

public class Soal01Jumlah {

    public static void Resolved() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Deret Angka : ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int jumlah = 0;

        for (int i = 0; i < length; i++) {
            jumlah = jumlah + intArray[i];
        }
        System.out.println(jumlah);
    }
}
