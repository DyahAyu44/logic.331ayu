package InputPlural;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        boolean flag = true;
        String answer = "y";

        while (flag) {
            System.out.println("Pilih Soal (1 - 12) : ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12) {
                System.out.println("Angka tidak tersedia");
                pilihan = input.nextInt();
            }

            switch (pilihan) {
                case 1:
                    Soal01Jumlah.Resolved();
                    break;
                case 2:
                    Soal02RataRata.Resolved();
                    break;
                case 3:
                    Soal03Median.Resolved();
                    break;
                case 4:
                    Soal04Modus.Resolved();
                    break;
                case 5:
                    Soal05Sort.Resolved();
                    break;
                case 6:
                    Soal06PasanganDeret.Resolve();
                    break;
                case 7:
                    Soal07JumlahTerbesar.Resolved();
                    break;
                case 8:
                    Soal08BanyakAngkaTerbesar.Resolve();
                    break;
                case 9:
                    Soal09Persentase.Resolve();
                    break;
                case 10:
                    Soal10MencariPrima.Resolve();
                    break;
                case 11:
                    Soal11ASCDES.Resolve();
                    break;
                case 12:
                    Soal12leaderboards.Resolve();
                    break;
                default:

            }
            System.out.println("Try again? y/n");
            input.nextLine(); //for skip
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")) {
                flag = false;
            }
        }
    }
}