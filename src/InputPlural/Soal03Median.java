package InputPlural;

import java.util.Arrays;
import java.util.Scanner;

public class Soal03Median {

    public static void Resolved() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Deret Angka : ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        double median = 0 ;

        Arrays.sort(intArray);

        if (length % 2 != 0)
        {
            median = intArray[length / 2];
            System.out.println(median);
        }
        else
        {
            median = (double) (intArray[(length - 1) / 2] + intArray[((length -1) / 2) + 1]) / 2;
            System.out.println(median);
        }
    }
}
