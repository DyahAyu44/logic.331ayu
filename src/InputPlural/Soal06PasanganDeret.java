package InputPlural;

import java.util.Scanner;

public class Soal06PasanganDeret {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan deret angka : ");
        String inputan = input.nextLine();

        int[] nilai = Utility.ConvertStringToArrayInt(inputan);
        int panjang = nilai.length;
        int counter = 0;
        int[] banyaknya = new int[panjang];
        boolean isSame = false;

        for (int i = 0; i <panjang ; i++) {
            if (i == 0){
                for (int j = 0; j < panjang; j++) {
                    if (nilai[i] == nilai[j]){
                        counter++;
                    }
                }
            }else {
                isSame = false;
                for (int j = 0; j < i; j++) {
                    if (nilai[i] == nilai[j]){
                        isSame = true;
                    }
                }
                if (isSame){
                    counter = 0;
                }else {
                    for (int j = 0; j <panjang ; j++) {
                        if (nilai[i] == nilai[j]){
                            counter ++;
                        }
                    }
                }
            }
            banyaknya[i] = counter;
            counter = 0;
        }
        int output = 0;
        for (int i = 0; i < panjang; i++) {
            output += banyaknya[i]/2;
        }

        System.out.println("Pasangan = " + output);
    }
}
