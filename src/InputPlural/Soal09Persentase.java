package InputPlural;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Soal09Persentase {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan deret angka = ");
        String text = input.nextLine();


        int[] intArray= Utility.ConvertStringToArrayInt(text);
        double length = intArray.length;
        double positif = 0;
        double negatif = 0;
        double zero = 0;

        for (int i = 0; i < length; i++) {
            if(intArray[i] > 0){
                positif++;
            } else if (intArray[i] < 0) {
                negatif++;
            } else if (intArray[i] == 0) {
                zero++;
            }
        }


        DecimalFormat desimal = new DecimalFormat("0.0");
        positif = ((positif/length) * 100);
        negatif = ((negatif/length) * 100);
        zero = ((zero/length) * 100);

        System.out.println(desimal.format(positif) + " %");
        System.out.println(desimal.format(negatif) + " %");
        System.out.println(desimal.format(zero) + " %");

    }
}
