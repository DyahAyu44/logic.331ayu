package InputPlural;

import java.util.Scanner;

public class Soal04Modus {

    public static void Resolved() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Deret Angka : ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        double jumlahAngka = 0, jumlahmodus = 0, modus = 0;
        for (int i = 0; i < intArray.length; i++) {
            for (int j = 0; j < intArray.length; j++) {
                if (intArray[i] == intArray[j] && i != j) {
                    jumlahAngka ++;
                }
            }
            if (jumlahAngka >= jumlahmodus) {
                jumlahmodus = jumlahAngka;
                modus = intArray[i];
                jumlahAngka = 0;
            }
        }

        System.out.println(modus);
    }
}
