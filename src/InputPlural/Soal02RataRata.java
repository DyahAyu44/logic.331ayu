package InputPlural;

import java.util.Scanner;

public class Soal02RataRata {

    public static void Resolved() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Deret Angka : ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        double jumlah = 0;

        for (int i = 0; i < length; i++) {
            jumlah = jumlah + intArray[i];
        }
        double ratarata = jumlah / length;
        System.out.println(ratarata);
    }
}
