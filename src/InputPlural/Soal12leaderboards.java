package InputPlural;

import java.util.Scanner;

public class Soal12leaderboards {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        String leaderboards, player;
        int temp = 0;

        System.out.print("Masukkan Point Leaderboards = ");
        leaderboards = input.nextLine();
        System.out.print("Masukkan Point Player = ");
        player = input.nextLine();

        int[] leaderboardsArray = Utility.ConvertStringToIntArray(leaderboards);
        int[] playerArray = Utility.ConvertStringToIntArray(player);

        int gabungan = leaderboardsArray.length + playerArray.length;
        int[] gabunganArray = new int[gabungan];

        int helper = 0;
        for (int gabung : leaderboardsArray) {
            gabunganArray[helper] = gabung;
            helper ++;
        }

        for (int gabung : playerArray) {
            gabunganArray[helper] = gabung;
            helper ++;
        }

        for (int i = 0; i < gabunganArray.length; i++) {
            for (int j = i+1; j < gabunganArray.length; j++) {
                if (gabunganArray[i] < gabunganArray[j])
                {
                    temp = gabunganArray[i];
                    gabunganArray[i] = gabunganArray[j];
                    gabunganArray[j] = temp;
                }
            }
        }
//        for (int i = 0; i < gabunganArray.length; i++) {
//            System.out.print(gabunganArray[i] + " ");
//        }
//        System.out.println();

        for (int i = 0; i < playerArray.length; i++) {
            for (int j = 0; j < gabunganArray.length; j++) {
                if (playerArray[i] == gabunganArray[j]) {
                    if (gabunganArray[j] == gabunganArray[j + 1]) {
                        System.out.print(j+1 + " ");
                        break;
                    } else {
                        System.out.print(j+1 + " ");
                    }
                }
            }
        }
        System.out.println();
    }
}
