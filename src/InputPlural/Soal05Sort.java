package InputPlural;

import java.util.Scanner;

public class Soal05Sort {

    public static void Resolved() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Deret Angka : ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int temp = 0;

        for (int i = 0; i < length; i++) {
            for (int j = i+1; j < length; j++) {
                if (intArray[i] > intArray[j])
                {
                    temp = intArray[i];
                    intArray[i] = intArray[j];
                    intArray[j] = temp;
                }
            }
        }

        for (int i = 0; i < length; i++) {
            System.out.print(intArray[i] + " ");
        }

        System.out.println();
    }
}