package FinalPractice;

import java.util.Scanner;

public class Soal05ConvertJam {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Konversi Jam");
        System.out.print("Masukkan Jam :");
        String jam = input.nextLine();

        int hh = Integer.parseInt(jam.substring(0,2));
        String mm = jam.substring(3,5);
        String ss = jam.substring(6,8);
        String format = jam.substring(9,10);

        if (format.equalsIgnoreCase("AM")) {
            if (hh == 12) {
                hh = 00;
            }
        } else {
            if (hh != 12) {
                hh += 12;
            }
        }

        System.out.println(hh + ":" + mm + ":" + ss);
    }
}
