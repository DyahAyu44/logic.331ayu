package FinalPractice;

import java.util.Scanner;

public class Soal03Rotasi {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Angka : ");
        String angka = input.nextLine();
        System.out.print("Dirotasi sebanyak : ");
        int rotasi = input.nextInt();

        String[] angkaSplit = angka.split(" ");

        for (int i = 0; i < rotasi; i++) {
            String temp = angkaSplit[0];
            for (int j = 0; j < angkaSplit.length - 1; j++) {
                angkaSplit[j] = angkaSplit[j + 1];
            }
            angkaSplit[angkaSplit.length - 1] = temp;
            System.out.println("Cetak Hasil Rotasi : ");
            for (int k = 0; k < angkaSplit.length; k++) {
                System.out.print(angkaSplit[k] + " ");
            }
            System.out.println();
        }
    }
}
