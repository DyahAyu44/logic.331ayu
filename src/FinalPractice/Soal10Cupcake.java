package FinalPractice;

import java.util.Scanner;

public class Soal10Cupcake {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Banyak Cupcake : ");
        int cupcake = input.nextInt();

        double terigu = (double) 125 / 15;
        double gula = (double) 100 / 15;
        double susu = (double) 100 /15;

        int jumTerigu = (int) (terigu * cupcake);
        int jumgula = (int) (gula * cupcake);
        int jumsusu = (int) (susu * cupcake);

        System.out.println("Untuk membuat cupcake sebanyak " + cupcake + " diperlukan terigu " + jumTerigu + " gr , gula pasir " + jumgula +
                " gr, susu " + jumsusu + " ml");
    }
}
