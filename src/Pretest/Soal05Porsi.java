package Pretest;

import java.util.Scanner;

public class Soal05Porsi {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        double porsi;

        System.out.print("Laki-Laki Dewasa : ");
        double lakilaki = input.nextDouble();
        System.out.print("Perempuan Dewasa : ");
        double perempuan = input.nextDouble();
        System.out.print("Remaja : ");
        double remaja = input.nextDouble();;
        System.out.print("Anak-Anak : ");
        double anak = input.nextDouble();
        System.out.print("Balita : ");
        double balita = input.nextDouble();

        double jumlah = lakilaki + perempuan + remaja + anak + balita;

        if (lakilaki > 0) {
            lakilaki *= 2;
        } else if (perempuan > 0) {
            perempuan *= 1;
        } else if (remaja > 0) {
            remaja *= 1;
        } else if (anak > 0) {
            anak = anak / 2;
        } else if (balita > 0) {
            balita *= 1;
        }

        if (jumlah >= 5 && jumlah % 2 == 1) {
            porsi = lakilaki + (perempuan + perempuan) + remaja + (anak/2) + balita;
        } else {
            porsi = lakilaki + perempuan + remaja + (anak/2) + balita;
        }

        System.out.println("Jumlah Porsi : " + porsi);
    }
}
