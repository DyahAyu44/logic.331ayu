package Pretest;

import java.util.Scanner;

public class Soal04JarakToko {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Jarak Toko ke Tempat : ");
        System.out.println("1. Toko ke Tempat 1 : 2 km");
        System.out.println("2. Tempat 1 ke Tempat 2 : 0.5 km");
        System.out.println("3. Tempat 2 ke Tempat 3 : 1.5 km");
        System.out.println("4. Tempat 3 ke Tempat 4 : 2.5 km");

        System.out.print("Masukkan Rute : ");
        String rute = input.nextLine();
        String jarak = ("0 2 0.5 1.5 2.5");
        double jarakTempuh = 0;
        double selisih;

        String[] ruteSplit = rute.split("-");
        int[] ruteArray = new int[ruteSplit.length];

        for (int i = 0; i < ruteSplit.length; i++) {
            ruteArray[i] = Integer.parseInt(ruteSplit[i]);
        }

        String[] jarakSplit = jarak.split(" ");
        double[] jarakArray = new double[jarakSplit.length];

        for (int i = 0; i < jarakSplit.length; i++) {
            jarakArray[i] = Double.parseDouble(jarakSplit[i]);
        }

        for (int i = 0; i < ruteArray.length; i++) {
            if (i == 0) {
                jarakTempuh = jarakArray[ruteArray[i]];
            } else if (i == ruteArray.length - 1) {
                selisih = jarakArray[ruteArray[i]] - jarakArray[ruteArray[i] - 1];
                if (selisih < 0) {
                    selisih *= -1;
                }
                jarakTempuh += (selisih + jarakArray[ruteArray[i]]);
            } else {
                selisih = jarakArray[ruteArray[i]] - jarakArray[ruteArray[i] - 1];
                if (selisih < 0) {
                    selisih *= -1;
                }
                jarakTempuh += selisih;
            }
        }

        System.out.println("Jarak yang ditempuh : " + jarakTempuh + " KM");
        double bensin = (jarakTempuh / 2.5);
        System.out.println("Bensin yang diperlukan : " + bensin + " liter");
    }
}
