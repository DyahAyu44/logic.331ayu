package Pretest;

import java.util.Scanner;

public class Soal03SiAngka1 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int n;

        System.out.print("Masukkan n : ");
        n = input.nextInt();

        int[] angka = new int[n];

        int helper = 0, helper1 = 0;

        for (int i = 0; i < n; i++) {
            angka[i] = 100 + helper1;
            helper ++;
            helper1 = (int) Math.pow(3, helper);
            System.out.println(angka[i] + " adalah 'Si Angka 1'");
        }
    }
}
