package Pretest;

import java.util.Scanner;

public class Soal01PrimaFibonaci {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        int[][] angka = new int[2][n];

        int[] angkaganjil = new int[n];
        int[] angkagenap = new int[n];
        int helper1 = 1;
        int helper2 = 2;

//        for (int i = 0; i < 2; i++) {
//            for (int j = 0; j < n; j+=2) {
//                if (i == 0) {
//                    angka[i][j] = helper1;
//                    helper1 += 2;
//                } else if (i == 1) {
//                    angka[i][j] = helper2;
//                    helper2 += 2;
//                }
//                System.out.print(angka[i][j] + " ");
//            }
//            System.out.println();
//        }

        for (int i = 0; i < n; i+=2) {
            angkaganjil[i] = helper1;
            helper1 += 2;
            System.out.print(angkaganjil[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < n; i+=2) {
            angkagenap[i] = helper2;
            helper2 += 2;
            if (n % 2 == 0) {
                if (angkagenap[i] <= n) {
                    System.out.print(angkagenap[i] + " ");
                }
            } else if (n % 2 == 1) {
                if (angkagenap[i] < n) {
                    System.out.print(angkagenap[i] + " ");
                }
            }
        }
        System.out.println();
    }
}
