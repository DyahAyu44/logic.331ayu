package Pretest;

import java.util.Scanner;

public class Soal08JumlahDeret {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        int[] prima = new int[n];
        int[] fibonaci = new int[n];
        int[] jumlah = new int[n];

        int helper = 1;
        for (int i = 0; i < n; i++) {
            boolean flag = true;
            while (flag) {
                if (helper == 1) {
                    helper++;
                } else if (helper == 2) {
                    prima[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper == 3) {
                    prima[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper % 2 != 0 && helper % 3 != 0) {
                    prima[i] = helper;
                    helper++;
                    flag = false;
                } else {
                    helper++;
                }
            }
            System.out.print(prima[i] + " ");
        }
        System.out.println();

        int helperfi =  1, before = 0, after = 1;
        for (int i = 0; i < n; i++) {
            fibonaci[i] = helperfi;
            helperfi = before + after;
            before = after;
            after = helperfi;

            System.out.print(fibonaci[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < n; i++) {
            jumlah[i] = prima[i] + fibonaci[i];
            System.out.print(jumlah[i] + " ");
        }
        System.out.println();
    }
}
