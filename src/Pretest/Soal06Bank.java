package Pretest;

import java.util.Scanner;

public class Soal06Bank {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String pinRegister, pinLogin;
        int pilihanMenu, pilihanTf, noRekekning, kodeBank;
        double saldo = 0, setor, transfer, biayaAdmin = 7.500;
        boolean flagLogin = true, flagMenu = true, flagTransfer = true;

        System.out.print("Harap masukan pin untuk registrasi: ");
        pinRegister = input.nextLine();

        while (flagLogin == true){
            System.out.print("Silahkan masukan pin untuk masuk: ");
            pinLogin = input.nextLine();

            if (pinLogin.equals(pinRegister)){
                while (flagMenu == true){
                    System.out.println("1. Setor Tunai");
                    System.out.println("2. Transfer");
                    System.out.println("3. Exit System");
                    System.out.print("Pilihan: ");
                    pilihanMenu = input.nextInt();

                    if (pilihanMenu == 1){
                        System.out.print("Masukan nominal setor: ");
                        setor = input.nextDouble();

                        saldo += setor;
                        System.out.println("Saldo anda: " + saldo);
                        flagMenu = true;
                    }
                    else if (pilihanMenu == 2) {
                        while (flagTransfer == true){
                            System.out.println("1. Antar Rekening");
                            System.out.println("2. Antar Bank");
                            System.out.println("3. Exit");
                            System.out.print("Pilihan: ");
                            pilihanTf = input.nextInt();

                            if (pilihanTf == 1){
                                System.out.print("Masukan rekening tujuan: ");
                                noRekekning = input.nextInt();

                                System.out.print("Masukan nominal transfer: ");
                                transfer = input.nextDouble();

                                if (transfer > saldo){
                                    System.out.println("Saldo anda tidak memenuhi!!!");
                                    flagTransfer = true;
                                }
                                else {
                                    saldo -= transfer;
                                    System.out.println("Transaksi berhasil!!!");
                                    System.out.println("Sisa saldo anda: " + saldo);
                                    flagTransfer = false;
                                }
                            }
                            else if (pilihanTf == 2) {
                                System.out.print("Masukan kode Bank: ");
                                kodeBank = input.nextInt();

                                System.out.print("Masukan nomor rekening: ");
                                noRekekning = input.nextInt();

                                System.out.print("Masukan nominal Transfer: ");
                                transfer = input.nextDouble();

                                if (transfer > saldo){
                                    System.out.println("Saldo anda tidak memenuhi!!!");
                                    flagTransfer = true;
                                }
                                else {
                                    saldo -= (transfer + biayaAdmin);
                                    System.out.println("Transaksi berhasil!!!");
                                    System.out.println("Sisa saldo anda: " + saldo);
                                    flagTransfer = false;
                                }
                            } else if (pilihanTf == 3) {
                                flagTransfer = false;
                            } else {
                                System.out.println("Pilihan tidak tersedia!!!");
                                flagTransfer = true;
                            }
                        }

                        flagTransfer = true;
                    }
                    else if (pilihanMenu == 3) {
                        System.out.println("Terimakasih sudah bertransaksi");
                        flagMenu = false;
                        flagLogin = false;
                    }
                    else {
                        System.out.println("Pilihan tidak tersedia");
                        flagMenu = true;
                    }
                }

                flagLogin = true;
            }
            else {
                System.out.println("Maaf, pin yang anda masukan salah!!!!!");
                flagLogin = true;
            }
        }
    }
}
