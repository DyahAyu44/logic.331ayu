package BankBBF;

import java.util.Scanner;
public class SetoranTunai
{
    private static Scanner input = new Scanner(System.in);

    private static String nominal;
    private static int saldo;
    private static int limit = 25000000;
    private static boolean flag = true;
    private static String answer = "y";
    public static void Setor() {

        while (flag) {
            System.out.print("Masukkan Nominal Uang : ");
            nominal = input.nextLine();

            if (Utility.IsNumeric(nominal)) {
                int tunai = Integer.parseInt(nominal);
                if (tunai <= limit) {
                    System.out.println("Are you sure? y/n");
                    answer = input.nextLine();
                    System.out.println("Setoran Anda " + nominal);
                    System.out.println("transaksi berhasil");
                    System.out.println("Apakah Ingin Transaksi Kembali?");
                    String jawab = input.nextLine();
                    if (jawab.toLowerCase().equals("y")) {
                        flag = false;
                        MasukkanPin.CekPin();
                    } else {
                        flag = false;
                        break;
                    }
                } else {
                    System.out.println("Nominal Uang yang Anda Masukkan Terlalu Banyak");
                }
            }
        }
    }
}
