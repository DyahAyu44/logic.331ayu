package BankBBF;

import java.util.Scanner;

public class Menu {
    private static Scanner input = new Scanner(System.in);
    public static void Menu() {



        System.out.println("Pilihan Menu : ");
        System.out.println("1. Setoran Tunai");
        System.out.println("2. Transfer");
        System.out.println();

        System.out.print("Pilih : ");
        int pilihan = input.nextInt();
        System.out.println();

        switch (pilihan) {
            case 1:
                SetoranTunai.Setor();
                break;
            case 2:
                Transfer.Transfer();
                break;
            default:
                System.out.println("Mohon Pilih Menu Yang Benar");
        }
    }
}
