package BankBBF;

import java.util.Scanner;

// buat pin
public class BuatPin
{
    private static Scanner input = new Scanner(System.in);
    private static String pin;
    private static String pin2;
    public static void BuatPin() {
        boolean flag = true;
        while (flag) {
            System.out.print("Buat PIN (6 Angka) : ");
            pin = input.nextLine();
            System.out.println();

            if (pin.length() != 6) {
                System.out.println("Angka yang anda masukkan kurang atau lebih dari 6!");
            } else {
                while (Utility.IsNumeric(pin)) {
                    System.out.print("Masukkan Ulang PIN Anda : ");
                    pin2 = input.nextLine();

                    if (isSame(pin, pin2)) {
                        flag = false;
                        break;
                    }
                }
            }
        }
    }

    public static String getPin() {
        return pin;
    }

    public static void setPin(String pin) {
        BuatPin.pin = pin;
    }

    public static boolean isSame(String pin, String pin2) {
        if (!pin.equals(pin2)) {
            System.out.println("Pin Tidak Sesuai");
            return false;
        }
        return true;
    }
}