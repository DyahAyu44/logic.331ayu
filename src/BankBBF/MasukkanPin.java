package BankBBF;

import java.util.Scanner;

public class MasukkanPin {
    private static Scanner input = new Scanner(System.in);
    private static boolean flag = false;
    private static int count = 0;
    public static void CekPin() {
        count = 0;
        flag = true;

        while (flag) {
            if (count == 3) {
                System.out.println("Akun Terblokir");
                flag = false;
                break;
            }

            System.out.print("Masukkan PIN : ");
            String pin = input.nextLine();
            if (pin.equals(BuatPin.getPin())) {
                flag = false;
                Menu.Menu();
            }

            if (!pin.equals(BuatPin.getPin())) {
                System.out.println("pin salah " + (2-count));
                count ++;
            }
        }
    }
}
