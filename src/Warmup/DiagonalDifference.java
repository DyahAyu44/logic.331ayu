package Warmup;

import Array2D.Utility;

import java.util.Scanner;

public class DiagonalDifference {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int primaryDiagonal = 0, seccondaryDiagonal = 0;

        System.out.print("Input panjang matrix : ");
        int n = input.nextInt();

        int[][] matrix = new int[n][n];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print("Matrix [" + (i+1) + "] [" + (j+1) + "] : " );
                matrix[i][j] = input.nextInt();
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

        int xPrimary = 0, yPrimary = 0;
        for (int i = 0; i < n; i++) {
            primaryDiagonal += matrix[xPrimary][yPrimary];
            xPrimary ++;
            yPrimary ++;
        }

        int ySeccondary = n-1, xSeccondary = 0;
        for (int i = 0; i < n; i++) {
            seccondaryDiagonal += matrix[xSeccondary][ySeccondary];
            xSeccondary ++;
            ySeccondary --;
        }

        int selisih = primaryDiagonal - seccondaryDiagonal;

        System.out.println("Primary Diagonal : " + primaryDiagonal);
        System.out.println("Seccondary Doagonal : " + seccondaryDiagonal);
        System.out.println("Selisih : " + Math.abs(selisih));
    }
}
