package Warmup;
import java.util.Scanner;
public class TimeConversion {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Jam : ");
        String inputTime = input.nextLine();

        String hasil;

        int hh = Integer.parseInt(inputTime.substring(0, 2));
        String mm = inputTime.substring(3, 5);
        String ss = inputTime.substring(6, 8);
        String period = inputTime.substring(8, 10);

        if (period.equalsIgnoreCase("AM")) {
            if (hh == 12) {
                hh = 0;
            }
        } else {
            if (hh != 12) {
                hh += 12;
            }
        }

        System.out.println(hh + ":" + mm + ":" + ss);
    }
}
