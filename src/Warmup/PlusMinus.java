package Warmup;

import DeretAngka.Utility;

import java.util.Scanner;

public class PlusMinus {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int n;

        double jumlahPositif, jumlahNegatif, jumlahNol;

        System.out.print("Input n : ");
        n = input.nextInt();
        System.out.print("Masukkan Angka : ");
        input.nextLine();
        String angka = input.nextLine();

        String[] angkaSplit = angka.split(" ");
        int[] angkaArray = new int[n];

        for (int i = 0; i < n; i++) {
            angkaArray[i] = Integer.parseInt(angkaSplit[i]);
        }

        int angkaPositif = 0, angkaNegatif = 0, angkaNol = 0;

        for (int i = 0; i < angkaArray.length; i++) {
            if (angkaArray[i] > 0) {
                angkaPositif ++;
            } else if (angkaArray[i] < 0) {
                angkaNegatif ++;
            } else {
                angkaNol ++;
            }
        }

        jumlahPositif = (double) angkaPositif / n;
        jumlahNegatif = (double) angkaNegatif / n;
        jumlahNol = (double) angkaNol / n;

        System.out.println("Positif : " + jumlahPositif);
        System.out.println("Negatif : " + jumlahNegatif);
        System.out.println("Nol : " + jumlahNol);
    }
}
