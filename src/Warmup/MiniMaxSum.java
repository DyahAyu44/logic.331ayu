package Warmup;

import InputPlural.Utility;

import java.util.Arrays;
import java.util.Scanner;

public class MiniMaxSum {

    public static void MiniMaxSum() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input Number : ");
        String text = input.nextLine();

        String[] textSplit = text.split(" ");
        int[] intArray = new int[textSplit.length];

        for (int i = 0; i < textSplit.length; i++) {
            intArray[i] = Integer.parseInt(textSplit[i]);
        }

        int length = intArray.length;
        int min = 0;
        int max = 0;

        Arrays.sort(intArray);
        for (int i = 0; i < length-1; i++) {
            min = min + intArray[i];
        }
        for (int i = 1; i < length; i++) {
            max = max + intArray[i];
        }

        System.out.println(min + " " + max);

//        System.out.print("Terbesar : " + max);
//        System.out.println();
//        System.out.println("Terkecil : " + min);
//        System.out.println();
    }
}
