package Warmup;

import java.util.Scanner;

public class BirthdayCakeCandles {

    public static void BirthdayCakeCandles() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();
        System.out.print("Masukkan Angka : ");
        input.nextLine();
        String angka = input.nextLine();

        String[] angkaSplit = angka.split(" ");
        int[] angkaArray = new int[n];

        for (int i = 0; i < n; i++) {
            angkaArray[i] = Integer.parseInt(angkaSplit[i]);
        }

        int maxHeight = -1;
        int count = 0;

        for (int i = 0; i < n; i++) {
            int height = angkaArray[i];
            if (height > maxHeight) {
                maxHeight = height;
                count = 1;
            } else if (height == maxHeight) {
                count++;
            }
        }
        System.out.println(count);
    }
}
