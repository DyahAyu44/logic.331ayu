package Warmup;

import Array2D.Utility;

import java.util.Scanner;

public class StairCase {

    public static void StairCase() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        int[][] hasil = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((j + i) >= n -1 )
                {
                    hasil[i][j] = 0;
                } else
                {
                    hasil[i][j] = 1;
                }
            }
        }
        Utility.Bintang(hasil);
    }
}
