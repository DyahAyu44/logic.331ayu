package Warmup;

import InputPlural.Utility;

import java.util.Scanner;

public class SimpleArraySum {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Angka : ");
        String angka = input.nextLine();

        String[] angkaSplit = angka.split(" ");
        int[] angkaArray = new int[angka.length()];

        for (int i = 0; i < angkaSplit.length; i++) {
            angkaArray[i] = Integer.parseInt(angkaSplit[i]);
        }

        int length = angkaArray.length;
        int jumlah = 0;

        for (int i = 0; i < length; i++) {
            jumlah += angkaArray[i];
        }

        System.out.print("Jumlah elemen array : " + jumlah);
        System.out.println();
    }
}
