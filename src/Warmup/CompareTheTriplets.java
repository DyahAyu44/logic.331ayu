package Warmup;

import InputPlural.Utility;

import java.util.Scanner;

public class CompareTheTriplets {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Alice : ");
        String alice = input.nextLine();
        System.out.print("Bob : ");
        String bob = input.nextLine();

        String[] aliceSplit = alice.split(" ");
        int[] aliceArray = new int[aliceSplit.length];

        for (int i = 0; i < aliceSplit.length; i++) {
            aliceArray[i] = Integer.parseInt(aliceSplit[i]);
        }

        String[] bobSplit = bob.split(" ");
        int[] bobArray = new int[bobSplit.length];

        for (int i = 0; i < bobSplit.length; i++) {
            bobArray[i] = Integer.parseInt(bobSplit[i]);
        }

        int aliceScore = 0, bobScrore = 0;

        for (int i = 0; i < aliceSplit.length; i++) {
            if (bobArray[i] < aliceArray[i]) {
                aliceScore ++;
            } else if (bobArray[i] > aliceArray[i]) {
                bobScrore ++;
            }
        }

        System.out.println("Alice : " + aliceScore + " " + "Bob : " + bobScrore);
    }
}
