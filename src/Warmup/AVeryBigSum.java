package Warmup;

import java.util.Scanner;

public class AVeryBigSum {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();
        System.out.print("Masukkan angka : ");
        input.nextLine();
        String angka = input.nextLine();

        String[] angkaSplit = angka.split(" ");
        long[] angkaArray = new long[n];

        for (int i = 0; i < n; i++) {
            angkaArray[i] = Long.parseLong(angkaSplit[i]);
        }

        long total = 0;

        for (int i = 0; i < angkaArray.length; i++) {
            total += angkaArray[i];
        }

        System.out.println("Ouput : " + total);
        System.out.println();
    }
}
