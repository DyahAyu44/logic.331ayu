package String;

import java.util.HashMap;
import java.util.Scanner;

public class MakingAnagrams {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int count = 0;

        System.out.print("Input 1: ");
        String s1 = input.nextLine();

        System.out.print("Input 2: ");
        String s2 = input.nextLine();

        char[] s1Char = s1.toCharArray();
        char[] s2Char = s2.toCharArray();

        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                for (int j = 0; j < s1Char.length; j++) {
                    if (s1Char[j] != 'c') {
                        count ++;
                    }
                }
            } else if (i == 1) {
                for (int j = 0; j < s2Char.length; j++) {
                    if (s2Char[j] != 'c') {
                        count ++;
                    }
                }
            }
        }
        System.out.println(count);
    }
}
