package String;

import java.util.Scanner;

public class TwoString {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int count = 0;

        System.out.print("String 1 : ");
        String string1 = input.nextLine();
        System.out.print("String 2 : ");
        String string2 = input.nextLine();

        char[] string1Char = string1.toCharArray();
        char[] string2Char = string2.toCharArray();

        for (int i = 0; i < string1Char.length; i++) {
            for (int j = 0; j < string2Char.length; j++) {
                if (string1Char[i] == string2Char[i]) {
                    count ++;
                }
            }
        }

        if (count != 0) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
