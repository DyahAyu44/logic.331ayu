package String;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrongPassword {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String password;
        boolean isStrong = true;

        while (isStrong){
            System.out.print("Password: ");
            password = input.nextLine();

            if (isPasswordValid(password)){
                System.out.println("Sukses membuat password");
                isStrong = false;
            }
            else {
                System.out.println("Gagal membuat password");
                isStrong = true;
            }
        }
    }

    public static boolean isPasswordValid(String password) {
        if (password.length() < 6 || password.length() > 6) {
            System.out.println("Message : Password harus 6 karakter");
            return false;
        }
        if (password.length() == 0) {
            System.out.println("Message: Password harus terisi minimal 1 karakter");
            return false;
        }

        if (!password.matches(".*[a-z].*")) {
            System.out.println("Message: Password harus terisi minimal 1 huruf kecil");
            return false;
        }

        if (!password.matches(".*[A-Z].*")) {
            System.out.println("Message: Password harus terisi minimal 1 huruf besar");
            return false;
        }

        Pattern specialCharPattern = Pattern.compile(".*[!@#$%^&*\\(\\)\\-\\+].*");
        Matcher matcher = specialCharPattern.matcher(password);
        if (!matcher.find()) {
            System.out.println("Message: Password harus terisi minimal 1 karakter");
            return false;
        }
        return true;
    }
}
