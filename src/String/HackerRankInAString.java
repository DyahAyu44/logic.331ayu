package String;

import java.util.Scanner;

public class HackerRankInAString {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        boolean isHackerRank = true;
        int count = 0;
        char[] keyChars = {'h', 'a', 'c', 'k', 'e', 'r', 'n'};

        System.out.print("Input: ");
        String text = input.nextLine();

        char[] chars = text.toCharArray();

        for (int i = 0; i < keyChars.length; i++) {
            for (int j = 0; j < chars.length; j++) {
                if (keyChars[i] == chars[j]){
                    count++;
                }
            }

            if (count == 0){
                isHackerRank = false;
                break;
            }
            else {
                isHackerRank = true;
            }

            count = 0;
        }

        if (isHackerRank == true){
            System.out.println("YA");
        }
        else {
            System.out.println("TIDAK");
        }
    }
}
