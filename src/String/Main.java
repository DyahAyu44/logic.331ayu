package String;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        boolean flag = true;
        String answer = "y";

        while (flag) {
            System.out.println("Choose a  Question (1 - 10) : ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12) {
                System.out.println("Option is not Available");
                pilihan = input.nextInt();
            }

            switch (pilihan) {
                case 1:
                    CamelCase.Resolve();
                    break;
                case 2:
                    StrongPassword.Resolve();
                    break;
                case 3:
                    CaesarChiper.Resolve();
                    break;
                case 4:
                    MarsExploration.Resolve();
                    break;
                case 5:
                    HackerRankInAString.Resolve();
                    break;
                case 6:
                    Pangrams.Resolve();
                    break;
                case 7:

                    break;
                case 8:
                    Gemstones.Resolve();
                    break;
                case 9:
                    MakingAnagrams.Resolve();
                    break;
                case 10:
                    TwoString.Resolve();
                    break;
                default:
            }
            System.out.println("Try again? y/n");
            input.nextLine(); //for skip
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")) {
                flag = false;
            }
        }
    }
}