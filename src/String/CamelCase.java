package String;

import java.util.Scanner;

public class CamelCase {

    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("For example = saveChangesInTheEditor");
        System.out.println("Make Your Sentence = ");
        String kalimat = input.nextLine();
        int count = 1;

        char[] huruf = kalimat.toCharArray();
        for (int i = 0; i < huruf.length; i++) {
            if (Character.isUpperCase(huruf[i]))
                count++;
        }
        System.out.println("the number of words you entered = " + count);
    }
}
