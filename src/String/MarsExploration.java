package String;

import java.util.Scanner;

public class MarsExploration {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("masukan kalimat SOS = ");
        String kalimat = input.nextLine();
        int result = 0;
        char[] kalimatSos = kalimat.toLowerCase().toCharArray();

        for (int i = 0; i < kalimatSos.length; i+=3) {
            if(kalimatSos[i] == 's' && kalimatSos[i + 1] == 'o' && kalimatSos[i + 2] == 's'){
                result = 0;
            } else {
                result ++;
            }
        }
        System.out.println(result);
    }
}
