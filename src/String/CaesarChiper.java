package String;

import java.util.Scanner;

public class CaesarChiper {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan kalimat = ");
        String kalimat = input.nextLine();
        System.out.println("masukan pindah berapa = ");
        int loncat =  input.nextInt();
        char[] hurufKalimat = kalimat.toCharArray();

        for (int i = 0; i < hurufKalimat.length; i++) {
            if (hurufKalimat[i] >= 'a' && hurufKalimat[i] <= 'z'){
                hurufKalimat[i] += loncat;
                if (hurufKalimat[i] > 'z'){
                    hurufKalimat[i] -= 26;
                }
            }
        }
        System.out.println(hurufKalimat);
    }
}
