package String;

import java.util.Scanner;

public class Gemstones
{
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input n : ");
        int n = input.nextInt();

        String[] gemstone = new String[n];

        for (int i = 0; i < gemstone.length; i++) {
            System.out.print("Gemstone: ");
            gemstone[i] = input.nextLine();
        }

        System.out.println("gemstone = ");
        for (int i = 0; i < gemstone.length; i++) {
            System.out.println(gemstone[i]);
        }
    }
}
